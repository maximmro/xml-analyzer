package com.agileengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class Main {

    private static final String DEFAULT_ELEMENT_ID = "make-everything-ok-button";

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        if (args.length < 2) {
            printRunHelpInfo();
            System.exit(0);
        }

        String originalFilePath = args[0];
        String sampleFilePath = args[1];
        String elementId = DEFAULT_ELEMENT_ID;

        if (args.length >= 3) {
            elementId = args[2];
        }

        LOGGER.info("Original file path: {}", originalFilePath);
        LOGGER.info("Sample file path: {}", sampleFilePath);
        LOGGER.info("Element id: {}", elementId);

        XmlAnalyzer xmlAnalyzer = new XmlAnalyzer();
        Optional<ElementMatch> matchingElement = xmlAnalyzer
                .findMatchElement(originalFilePath, sampleFilePath, elementId);

        if (matchingElement.isPresent()) {
            ElementMatch matchElement = matchingElement.get();
            LOGGER.info("\n\nFound element, cssSelector:\n {}", matchElement.getElementHolder().getElement().cssSelector());
            LOGGER.info("Match score: {}%", matchElement.getMatchScore());
            LOGGER.info("Element was selected because of:");
            matchElement.getMatchAnalysis().forEach(a -> LOGGER.info("\t" + a));
        } else {
            LOGGER.error("Element was not found");
        }
    }

    private static void printRunHelpInfo() {
        System.out.println("" +
                "Smart XML analyzer test app.\n" +
                "\n" +
                "Usage: xml-analyzer <input_origin_file_path> <input_other_sample_file_path> [origin-element-id]\n" +
                "\n" +
                "    input_origin_file_path          - full file path to original HTML file\n" +
                "    input_other_sample_file_path    - full file path to sample HTML file\n" +
                "    origin-element-id               - optional target element id, if omitted the default value is: sendMessageButton\n" +
                "\n");
    }

}
