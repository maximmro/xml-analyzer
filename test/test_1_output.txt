C:\Users\mrybal\src\AgileEngine\xml-analyzer\test>java -jar xml-analyzer.jar ..\samples\sample-0-origin.html ..\samples\sample-1-evil-gemini.html
[INFO] 2018-12-24 06:26:53,406 c.a.Main - Original file path: ..\samples\sample-0-origin.html
[INFO] 2018-12-24 06:26:53,408 c.a.Main - Sample file path: ..\samples\sample-1-evil-gemini.html
[INFO] 2018-12-24 06:26:53,408 c.a.Main - Element id: make-everything-ok-button
[INFO] 2018-12-24 06:26:53,539 c.a.XmlAnalyzer - Looking for sample elements with css query: div[class="col-lg-8"] a[class*="btn"]
[INFO] 2018-12-24 06:26:53,558 c.a.Main -

Found element, cssSelector:
 #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > a.btn.btn-success
[INFO] 2018-12-24 06:26:53,559 c.a.Main - Match score: 62%
[INFO] 2018-12-24 06:26:53,559 c.a.Main - Element was selected because of:
[INFO] 2018-12-24 06:26:53,559 c.a.Main -       Identical css class : btn-success
[INFO] 2018-12-24 06:26:53,559 c.a.Main -       Identical css class : btn
[INFO] 2018-12-24 06:26:53,559 c.a.Main -       Identical tagValue : Make everything OK
[INFO] 2018-12-24 06:26:53,560 c.a.Main -       Identical onclick : javascript:window.okDone(); return false;
[INFO] 2018-12-24 06:26:53,560 c.a.Main -       Identical title : Make-Button

